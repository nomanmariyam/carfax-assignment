package com.carfax.vehicle.service.api;

import javax.validation.constraints.NotBlank;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carfax.vehicle.service.api.response.VehicleRecords;
import com.carfax.vehicle.service.api.response.exception.VehicleServiceException;

@Validated
@RestController
@RequestMapping(value="/vehicle-data-service")
public interface VehicleDataService {
	
	@GetMapping("/historydata")
	VehicleRecords getVehicleData(@NotBlank String vin) throws VehicleServiceException;
	
}
