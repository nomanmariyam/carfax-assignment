package com.carfax.vehicle.service.api.response.exception;


public class VehicleServiceException extends Exception {

    private static final long serialVersionUID = 1L;
    
    private String action;

    public VehicleServiceException(String action, String message, Throwable cause) {
        super(message, cause);
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    /**
     * Constructor to use when another exception should be wrapped.
     * @param t The exception to wrap.
     */
    public VehicleServiceException(Throwable t) {
        super(t);
    }

    /**
     * Default constructor.
     */
    public VehicleServiceException() {
        super();
    }

    /**
     * Constructor to use when the exception should contain additional info.
     * @param message The additional info to include.
     */
    public VehicleServiceException(String message) {
        super(message);
    }

    /**
     * Constructor to wrap an exception and include additional info.
     * @param message The additional info to include.
     * @param t The exception to wrap.
     */
    public VehicleServiceException(String message, Throwable t) {
        super(message, t);
    }
}
