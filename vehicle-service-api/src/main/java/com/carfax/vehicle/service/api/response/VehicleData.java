package com.carfax.vehicle.service.api.response;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VehicleData implements Serializable {
	
	private static final long serialVersionUID = 3526078828692303449L;

	private String vin;
	
	private LocalDate date;
	
	@JsonProperty("data_provider_id")
	private Integer dataProviderId;
	
	@JsonProperty("odometer_reading")
	private Integer odometerReading;
	
	@JsonProperty("service_details")
	private List<String> serviceDetails;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("odometer_rollback")
	private Boolean odometerRollback;
	
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("milage_inconsistency")
	private Boolean milageInconsitency;
	
	public VehicleData() {
		super();
	}
	
	public VehicleData(String vin, LocalDate date, Integer odometerReading) {
		super();
		this.vin = vin;
		this.date = date;
		this.odometerReading = odometerReading;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Integer getDataProviderId() {
		return dataProviderId;
	}

	public void setDataProviderId(Integer dataProviderId) {
		this.dataProviderId = dataProviderId;
	}

	public Integer getOdometerReading() {
		return odometerReading;
	}

	public void setOdometerReading(Integer odometerReading) {
		this.odometerReading = odometerReading;
	}

	public List<String> getServiceDetails() {
		return serviceDetails;
	}

	public void setServiceDetails(List<String> serviceDetails) {
		this.serviceDetails = serviceDetails;
	}

	public Boolean getOdometerRollback() {
		return odometerRollback;
	}

	public void setOdometerRollback(Boolean odometerRollback) {
		this.odometerRollback = odometerRollback;
	}
	
	public Boolean getMilageInconsitency() {
		return milageInconsitency;
	}

	public void setMilageInconsitency(Boolean milageInconsitency) {
		this.milageInconsitency = milageInconsitency;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataProviderId == null) ? 0 : dataProviderId.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((milageInconsitency == null) ? 0 : milageInconsitency.hashCode());
		result = prime * result + ((odometerReading == null) ? 0 : odometerReading.hashCode());
		result = prime * result + ((odometerRollback == null) ? 0 : odometerRollback.hashCode());
		result = prime * result + ((serviceDetails == null) ? 0 : serviceDetails.hashCode());
		result = prime * result + ((vin == null) ? 0 : vin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VehicleData other = (VehicleData) obj;
		if (dataProviderId == null) {
			if (other.dataProviderId != null)
				return false;
		}
		else if (!dataProviderId.equals(other.dataProviderId))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		}
		else if (!date.equals(other.date))
			return false;
		if (milageInconsitency == null) {
			if (other.milageInconsitency != null)
				return false;
		}
		else if (!milageInconsitency.equals(other.milageInconsitency))
			return false;
		if (odometerReading == null) {
			if (other.odometerReading != null)
				return false;
		}
		else if (!odometerReading.equals(other.odometerReading))
			return false;
		if (odometerRollback == null) {
			if (other.odometerRollback != null)
				return false;
		}
		else if (!odometerRollback.equals(other.odometerRollback))
			return false;
		if (serviceDetails == null) {
			if (other.serviceDetails != null)
				return false;
		}
		else if (!serviceDetails.equals(other.serviceDetails))
			return false;
		if (vin == null) {
			if (other.vin != null)
				return false;
		}
		else if (!vin.equals(other.vin))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "VehicleData [vin=" + vin + ", date=" + date + ", dataProviderId=" + dataProviderId
				+ ", odometerReading=" + odometerReading + ", serviceDetails=" + serviceDetails + ", odometerRollback="
				+ odometerRollback + ", milageInconsitency=" + milageInconsitency + "]";
	}
}
