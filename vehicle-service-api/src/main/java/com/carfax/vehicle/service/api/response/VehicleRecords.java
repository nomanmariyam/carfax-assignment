package com.carfax.vehicle.service.api.response;

import java.io.Serializable;
import java.util.List;

public class VehicleRecords implements Serializable {
	
	private static final long serialVersionUID = 1832456666487486207L;
	
	private List<VehicleData> records;

	public List<VehicleData> getRecords() {
		return records;
	}

	public void setRecords(List<VehicleData> records) {
		this.records = records;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((records == null) ? 0 : records.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VehicleRecords other = (VehicleRecords) obj;
		if (records == null) {
			if (other.records != null)
				return false;
		}
		else if (!records.equals(other.records))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "VehicleRecords [records=" + records + "]";
	}
	
}
