package com.carfax.vehicle.service.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication(scanBasePackages = {"com.carfax.vehicle.service.api.*"})
@ImportResource({"classpath*:vehicle-service-app-context.xml"})
public class VehicleServiceApplication {

	public static void main(String[] args) {
        SpringApplication.run(VehicleServiceApplication.class, args);
    }
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
	   return builder.build();
	}
}
