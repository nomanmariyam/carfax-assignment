package com.carfax.vehicle.service.impl.data;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

import com.carfax.vehicle.service.api.response.VehicleRecords;
import com.carfax.vehicle.service.api.response.exception.VehicleServiceException;

public class VehicleDataProvider {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${dataProvider.url}")
	private String dataProviderUrl;
	
	public VehicleRecords getVehicleDataByVehicleNumber(String vin) throws VehicleServiceException {
		try {
			return restTemplate.getForObject(new URI(getFinalUrl(vin)), VehicleRecords.class);
		} catch (Exception e) {
			logger.error("Exception while calling vehicle dataProvider: ", e);
			throw new VehicleServiceException("Exception while calling vehicle dataprovider: " + e.getMessage());
		}
	}

	protected String getFinalUrl(String vin) {
		return dataProviderUrl.trim().concat(vin+".json");
	}
}
