package com.carfax.vehicle.service.impl.exception;

import java.time.LocalDateTime;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.carfax.vehicle.service.api.response.exception.ExceptionDetails;
import com.carfax.vehicle.service.api.response.exception.VehicleServiceException;

@ControllerAdvice
public class VehicleServiceExceptionHandler {
	
	@ExceptionHandler(value = ConstraintViolationException.class)
	protected ResponseEntity<ExceptionDetails> handleBadRequest(ConstraintViolationException ex,
			WebRequest request) {
		ExceptionDetails errorDetails = new ExceptionDetails(
				LocalDateTime.now(), 
				HttpStatus.BAD_REQUEST.value(),
				HttpStatus.BAD_REQUEST.getReasonPhrase(),
				ex.getMessage(),
				request.getDescription(false));
		return new ResponseEntity<ExceptionDetails>(errorDetails, HttpStatus.BAD_REQUEST);
	}
	
	
	@ExceptionHandler(value = VehicleServiceException.class)
	protected ResponseEntity<ExceptionDetails> handleVehicleServiceException(VehicleServiceException ex,
			WebRequest request) {
		ExceptionDetails errorDetails = new ExceptionDetails(
				LocalDateTime.now(), 
				HttpStatus.INTERNAL_SERVER_ERROR.value(),
				HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
				ex.getMessage(),
				request.getDescription(false));
		return new ResponseEntity<ExceptionDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
