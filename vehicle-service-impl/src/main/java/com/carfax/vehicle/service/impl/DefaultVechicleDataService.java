package com.carfax.vehicle.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;

import com.carfax.vehicle.service.api.VehicleDataService;
import com.carfax.vehicle.service.api.response.VehicleData;
import com.carfax.vehicle.service.api.response.VehicleRecords;
import com.carfax.vehicle.service.api.response.exception.VehicleServiceException;
import com.carfax.vehicle.service.impl.data.VehicleDataProvider;

public class DefaultVechicleDataService implements VehicleDataService {

	@Autowired
	private VehicleDataProvider vehicleDataProvider;
	
	@Override
	public VehicleRecords getVehicleData(@RequestParam String vin) throws VehicleServiceException {
		VehicleRecords vehicleRecords = vehicleDataProvider.getVehicleDataByVehicleNumber(vin);
		if(vehicleRecords != null) {
			identifyOdometerRollbackOrMilageInconsistency(vehicleRecords.getRecords());
		}
		
		return vehicleRecords;
	}

	protected void identifyOdometerRollbackOrMilageInconsistency(List<VehicleData> vehicleDataList) {
		if(vehicleDataList != null && vehicleDataList.size() > 1) {
			vehicleDataList.sort((o1,o2) -> o1.getDate().compareTo(o2.getDate()));
			
			for(int i= 0; i < vehicleDataList.size() - 1 ; i++) {
				
				VehicleData currentRecord = vehicleDataList.get(i);
				VehicleData nextRecord = vehicleDataList.get(i+1);
				
				if(currentRecord.getOdometerReading() >= nextRecord.getOdometerReading()
						&& nextRecord.getDate().compareTo(currentRecord.getDate()) > 0) {
					if(nextRecord.getMilageInconsitency() == null) {
						nextRecord.setOdometerRollback(true);
					}
				}
			}

			for(int i= 0; i < vehicleDataList.size() - 1 ; i++) {
				
				VehicleData currentRecord = vehicleDataList.get(i);
				VehicleData nextRecord = vehicleDataList.get(i+1);
				
				if(currentRecord.getOdometerRollback() != null 
						&& currentRecord.getOdometerRollback()) {
					if(nextRecord.getOdometerReading() >=  currentRecord.getOdometerReading()) {
						currentRecord.setMilageInconsitency(true);
						currentRecord.setOdometerRollback(null);
					}
				}
			}
			
			
		}
	}
}
