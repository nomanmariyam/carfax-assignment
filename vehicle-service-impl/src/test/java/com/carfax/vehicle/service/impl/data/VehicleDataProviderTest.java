package com.carfax.vehicle.service.impl.data;

import static org.junit.Assert.*;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.carfax.vehicle.service.api.response.VehicleRecords;
import com.carfax.vehicle.service.api.response.exception.VehicleServiceException;

@RunWith(MockitoJUnitRunner.class)
public class VehicleDataProviderTest {
	
	@InjectMocks
	private VehicleDataProvider vehicleDataProvider;
	
	@Mock
	private RestTemplate restTemplate;
	
	@Before
	public void init() {
		ReflectionTestUtils.setField(vehicleDataProvider, "dataProviderUrl", "url/");
	}
	
	@Test
	public void testGetVehicleDataByVehicleNumberSuccess() throws VehicleServiceException, RestClientException, URISyntaxException {
		URI url = new URI(vehicleDataProvider.getFinalUrl("vin123"));
		
		Mockito.when(restTemplate.getForObject(
				url, VehicleRecords.class))
				.thenReturn(new VehicleRecords());
		
		assertNotNull(vehicleDataProvider.getVehicleDataByVehicleNumber("vin123"));
		
		Mockito.verify(restTemplate, Mockito.times(1)).getForObject(url, VehicleRecords.class);
	}
	
	@Test(expected=VehicleServiceException.class)
	public void testGetVehicleDataByVehicleNumberException() throws VehicleServiceException, RestClientException, URISyntaxException {
		URI url = new URI(vehicleDataProvider.getFinalUrl("vin123"));
		
		Mockito.when(restTemplate.getForObject(
				url, VehicleRecords.class))
				.thenThrow(new RuntimeException("exception"));
		
		vehicleDataProvider.getVehicleDataByVehicleNumber("vin123");
		
		Mockito.verify(restTemplate, Mockito.times(1)).getForObject(url, VehicleRecords.class);
	}
	
	@Test
	public void testGetFinalUrl() throws VehicleServiceException {
		assertEquals("url/vin123.json", vehicleDataProvider.getFinalUrl("vin123"));
	}
	
}
