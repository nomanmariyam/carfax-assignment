package com.carfax.vehicle.service.impl;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.carfax.vehicle.service.api.response.VehicleData;
import com.carfax.vehicle.service.api.response.VehicleRecords;
import com.carfax.vehicle.service.api.response.exception.VehicleServiceException;
import com.carfax.vehicle.service.impl.data.VehicleDataProvider;

@RunWith(MockitoJUnitRunner.class)
public class DefaultVechicleDataServiceTest {
	
	@InjectMocks
	private DefaultVechicleDataService vehicleDataService;
	
	@Mock
	private VehicleDataProvider vehicleDataProvider;
	
	@Test
	public void testGetVehicleDataSuccess() throws VehicleServiceException {
		Mockito.when(vehicleDataProvider.getVehicleDataByVehicleNumber("vin1")).thenReturn(
				prepareVehicleDataRecords());
		
		VehicleRecords fethcedRecords = vehicleDataService.getVehicleData("vin1");
		assertEquals(1, fethcedRecords.getRecords().size());
		
		Mockito.verify(vehicleDataProvider, Mockito.times(1)).getVehicleDataByVehicleNumber("vin1");
	}
	
	@Test(expected=VehicleServiceException.class)
	public void testGetVehicleDataException() throws VehicleServiceException {
		Mockito.when(vehicleDataProvider.getVehicleDataByVehicleNumber("vin1"))
			.thenThrow(new VehicleServiceException("exception"));
		
		vehicleDataService.getVehicleData("vin1");
		
		Mockito.verify(vehicleDataProvider, Mockito.times(1)).getVehicleDataByVehicleNumber("vin1");
	}

	@Test
	public void testIdentifyOdometerRollback() {
		//if list is null
		vehicleDataService.identifyOdometerRollbackOrMilageInconsistency(null);
		
		//if list is empty
		List<VehicleData> vehicleData = new ArrayList<VehicleData>();
		vehicleDataService.identifyOdometerRollbackOrMilageInconsistency(vehicleData);
		assertEquals(0, vehicleData.size());
		
		//if list contains only 1 record
		vehicleData.add(new VehicleData("vin1", LocalDate.parse("2018-04-01"), 5600));
		vehicleDataService.identifyOdometerRollbackOrMilageInconsistency(vehicleData);
		assertEquals(1, vehicleData.size());
		assertNull(vehicleData.get(0).getOdometerRollback());
		
		//if list contains 2 record and for one of them is odometer rollback
		vehicleData.add(new VehicleData("vin2", LocalDate.parse("2018-02-12"), 15100));
		vehicleDataService.identifyOdometerRollbackOrMilageInconsistency(vehicleData);
		assertEquals(2, vehicleData.size());
		assertNull(vehicleData.get(0).getOdometerRollback());
		assertTrue(vehicleData.get(1).getOdometerRollback());
	}
	
	@Test
	public void testIdentifyOdometerRollback1() {
		//if list is empty
		List<VehicleData> vehicleData = new ArrayList<VehicleData>();
		vehicleData.add(new VehicleData("vin1", LocalDate.parse("2017-01-02"), 12200));
		vehicleData.add(new VehicleData("vin1", LocalDate.parse("2017-01-02"), 12100));
		
		vehicleDataService.identifyOdometerRollbackOrMilageInconsistency(vehicleData);
		assertEquals(2, vehicleData.size());
		assertNull(vehicleData.get(0).getOdometerRollback());
		assertNull(vehicleData.get(1).getOdometerRollback());
	}
	
	
	@Test
	public void testIdentifyMilgaeInconsistency() {
		//if list is empty
		List<VehicleData> vehicleData = new ArrayList<VehicleData>();
		vehicleData.add(new VehicleData("vin1", LocalDate.parse("2017-01-01"), 12200));
		vehicleData.add(new VehicleData("vin1", LocalDate.parse("2017-01-02"), 12000));
		vehicleData.add(new VehicleData("vin1", LocalDate.parse("2017-01-03"), 12300));
		
		vehicleDataService.identifyOdometerRollbackOrMilageInconsistency(vehicleData);
		assertEquals(3, vehicleData.size());
		assertNull(vehicleData.get(0).getOdometerRollback());
		
		assertNull(vehicleData.get(1).getOdometerRollback());
		assertTrue(vehicleData.get(1).getMilageInconsitency());
		
		assertNull(vehicleData.get(2).getOdometerRollback());
	}
	
	private VehicleRecords prepareVehicleDataRecords() {
		VehicleRecords vehicleRecords = new VehicleRecords();
		List<VehicleData> vehicleData = new ArrayList<VehicleData>();
		vehicleData.add(new VehicleData("vin1", LocalDate.parse("2018-04-01"), 5600));
		vehicleRecords.setRecords(vehicleData);
		return vehicleRecords;
	}
	
}
