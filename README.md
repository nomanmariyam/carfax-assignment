## How to run the program
#### Prerequisite
	1. MAVEN
	2. JAVA8
#### Clone the repo
```sh
$ git clone https://nomanmariyam@bitbucket.org/nomanmariyam/carfax-assignment.git
```
#### Build the project
```sh
$ cd carfax-assignment 
$ mvn clean install
```
#### Start the service
```sh
$ java -jar vehicle-service-app/target/vehicle-service-app.jar
```
 Default port is 8080, to change it execute the below command
 
```sh
$ java -jar vehicle-service-app/target/vehicle-service-app.jar --server.port=8090
```
If there is change in dataprovider url , execute the below command

```sh
$ java -jar vehicle-service-app/target/vehicle-service-app.jar --dataProvider.url=https://s3-eu-west-1.amazonaws.com/coding-challenge.carfax.eu/
```
#### Test the service
http://localhost:8080/vehicle-data-service/historydata?vin=VSSZZZ6JZ9R056308
-----------------------------------------
### Docker 
Alternate there is also docker image available in docker hub
#### Get the image
```sh
$ docker pull nomanmariyam/vehicle-service-app:R1.0-SNAPSHOT
```
#### Start the container
```sh
$ docker run -p 8080:8080  nomanmariyam/vehicle-service-app:R1.0-SNAPSHOT
```
If there is change in dataprovider url
```sh
$ docker run -p 8080:8080  -e DATAPROVIDER_URL=https://s3-eu-west-1.amazonaws.com/coding-challenge.carfax.eu/ nomanmariyam/vehicle-service-app:R1.0-SNAPSHOT
```	
#### Test the Service
http://localhost:8080/vehicle-data-service/historydata?vin=VSSZZZ6JZ9R056308

-----------
### The project has been implemented using spring-boot, contains below 3 modules

#### vehicle-service-api  
  interface containing service endpoint and response models
#### vehicle-service-impl 
service implementation and data-provider consumer
#### vehicle-service-app
App entry point, spring boot configuration